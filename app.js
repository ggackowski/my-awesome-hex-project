
const log = console.log;
const app = new PIXI.Application({transparent: true, width: 1000, height: 800});

const PI = 3.14; 
const size = 50;
const map_x = 4;
const map_y = 4;

function flat_hex_corner(center, size, i) {
    var angle_deg = 60 * i - 60;
    var angle_rad = PI / 180 * angle_deg;
    return [center[0] + size * Math.cos(angle_rad), center[1] + size * Math.sin(angle_rad)];
}

function flat_hex_to_pixel(hex) {
    var x = size * (3./2 * hex.q);
    var y = size * (Math.sqrt(3)/2 * hex.q  +  Math.sqrt(3) * hex.r);
    return [x + size/2, y + size/2];
}

function pixel_to_flat_hex(point) {
    var q = ( 2./3 * point.x) / size;
    var r = (-1./3 * point.x  +  Math.sqrt(3)/3 * point.y) / size;
    return {r: Math.round(r), q: Math.round(q)};
}
    
function Style(a, b, c) {
    return {a: a, b: b, c: c};
}

function makeHexPos(pos, style)
{
    let Hex = new PIXI.Graphics;
    
    let polygon = [
        flat_hex_corner(flat_hex_to_pixel(pos), size, 0)[0], flat_hex_corner(flat_hex_to_pixel(pos), size, 0)[1],
        flat_hex_corner(flat_hex_to_pixel(pos), size, 1)[0], flat_hex_corner(flat_hex_to_pixel(pos), size, 1)[1],
        flat_hex_corner(flat_hex_to_pixel(pos), size, 2)[0], flat_hex_corner(flat_hex_to_pixel(pos), size, 2)[1],
        flat_hex_corner(flat_hex_to_pixel(pos), size, 3)[0], flat_hex_corner(flat_hex_to_pixel(pos), size, 3)[1],
        flat_hex_corner(flat_hex_to_pixel(pos), size, 4)[0], flat_hex_corner(flat_hex_to_pixel(pos), size, 4)[1],
        flat_hex_corner(flat_hex_to_pixel(pos), size, 5)[0], flat_hex_corner(flat_hex_to_pixel(pos), size, 5)[1],
        flat_hex_corner(flat_hex_to_pixel(pos), size, 0)[0], flat_hex_corner(flat_hex_to_pixel(pos), size, 0)[1],
    ];
    Hex.lineStyle(style.a, style.b, style.c);
    Hex.drawPolygon(polygon);
    return Hex;
}

document.getElementById('display').appendChild(app.view);

let hexs = [];
for (let i = 0; i <= map_x; ++i) {
    hexs.push([]);
    for (let j = 0; j <= map_y; ++j) {
        hexs[i].push({r: i, q: j + 3}); 
    }
}

for (let i = 0; i < hexs.length; ++i) {
    for (let j = 0; j < hexs[i].length; ++j) {
        app.stage.addChild(makeHexPos(hexs[i][j], Style(3, 0xFF067F , 1)));
        log(hexs[i][j]);
    }
}

const getMousePosition = () => app.renderer.plugins.interaction.mouse.global;

//log(pixel_to_flat_hex(getMousePosition()));


app.ticker.add(
    () => { log(pixel_to_flat_hex(getMousePosition())) }
); 
//0xFF067F